# star_wars_game
 
This is a quick choose-your-own-adventure game based off of Star Wars written in Python. Depending on choices made, might take 10 minutes max for one full run through.\
Any information given is kept locally during gameplay only.

## Installation
To use:
1. download the zip file
1. extract the files
1. run the application (Star_Wars_Adventure.exe) file

## How to Use
1. You will be presented with a text box to enter you name. Be sure to click the box before typing.
1. All remaining actions will be buttons that can be clicked with your mouse.
1. For all mini-games, follow the on screen directions.

## Licensing
All images and information was acquired online.\
All sound effects were acquired from [YouTube](https://www.youtube.com).\
This is a personal project and was not built for distribution purposes.
